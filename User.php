<?

class User extends Entity
{
    private $_options = array();
    
    public function inicialize()
    {
        session_start();
        
        if (!array_key_exists('options', $_SESSION)) {
            $_SESSION['options'] = array();
        }
        
        $this->_options = &$_SESSION['options'];
        
        return true;
    } // end inicialize
    
    public function setOption($optionName, $value)
    {
        $this->_options[$optionName] = $value;
        
        return true;
    } // setOption
    
    public function getOption($optionName)
    {
        if (!array_key_exists($optionName, $this->_options)) {
            return false;
        }
        
        return $this->_options[$optionName];
    } // end getOption
    
    public function authorization(
        $userId, $login, $type, $groups = array()
    )
    {
        $this->setOption('user_id', $userId);
        $this->setOption('user_login', $login);
        $this->setOption('user_type', $type);
        $this->setOption('signed', true);
        $this->setOption('groups', $groups);
        
        return true;
    } // end authorization
    
    public function isSigned()
    {
        return $this->getOption('signed');
    } // end isSigned
    
    public function getLogin()
    {
        return $this->getOption('user_login');
    } // end getLogin
    
    public function getType()
    {
        return $this->getOption('user_type');
    } // end getType
    
    public function hasAccessToGroups($groups)
    {
        if (!$groups) {
            return true;
        }
        
        $userGroups = $this->getOption("groups");
        
        if (!$userGroups) {
            return false;
        }
        
        if (!is_array($groups)) {
            return in_array($gropus, $userGroups);
        }
        
        return array_intersect($userGroups, $groups);
    } // end hasMemberToGroups
    
    public function exitSession()
    {
        session_unset();
        
        return true;
    }
}