<?php 

interface ISqlTool
{
	public function query($sql, $sqlCondition = array());
	public function insert($table, $values = array());
	public function remove($table, $sqlCondition = array());
	public function update($table, $values = array() , $sqlCondition = array());
	public function massInsert($table, $values = array());
	public function quote($value);
}