<?php 

class MysqlTool implements ISqlTool
{
	private $_dbServer;
	private $_dbUser;
	private $_dbPassword;
	private $_dbName;
	
	public function __construct(Config $config)
	{
		$this->_dbServer   = $config->dbServer;
		$this->_dbUser     = $config->dbUser;
		$this->_dbPassword = $config->dbPassword;
		$this->_dbName     = $config->dbName;
	} // end __construct
	
    public function query($sql, $sqlCondition = array())
    {
    	return $this->_dbQuery($sql);
    } // end query
    
	public function insert($table, $values = array())
	{
		$sql = "INSERT INTO " . $table . "(";
		
		foreach ($values as $colName => $colValue) {
			$sql .= " " . $colName . ",";
		}
		
		$sql = substr($sql, 0, -1);
		
		$sql .= "VALUES (";
		
		foreach ($values as $colValue) {
			$sql .= " " . $this->quote($colValue) . ",";
		}
		
		$sql = substr($sql, 0, -1);
		$sql .= ")";
		
		return $this->_dbQuery($sql);
	} // end insert
	
	
	public function remove($table, $sqlCondition = array())
	{
		$sql = "DELETE FROM " . $table;
		
		if ($sqlCondition) {
			$sql .= $this->_getSqlCondition();
		}
		
		return $this->_dbQuery($sql);
	} // end remove
	
	public function update($table, $values = array() , $sqlCondition = array())
	{
		$sql = "UPDATE " . $table . " SET ";
		
		foreach ($values as $colName => $colValue) {
			$sql .= " " . $colName . "=" . $this->quote($colValue) . ",";
		}
		
		$sql = substr($sql, 0, -1);
		
		if ($sqlCondition) {
			$sql .= $this->_getSqlCondition($sqlCondition);
		}
		
		return $this->_dbQuery($sql);
	} // end update
	
	public function massInsert($table, $values = array())
	{
		$sql = "INSERT INTO " . $table . "(";
		
		foreach ($values as $colName => $colValue) {
			$sql .= " " . $colName . ",";
		}
		
		$sql = substr($sql, 0, -1);
		
		foreach ($values as $row) {
			$sql .= " (";
			foreach ($row as $value) {
				$sql .= " " . $value . ",";
			}
			$sql = substr($sql, 0, -1);
			$sql .= "),";
		}
		
		$sql = substr($sql, 0, -1);
		
		return $this->_dbQuery($sql);
	} // end massInsert
	
	public function quote($value)
	{
		return "'" . $value . "'"; // need normal quote
	} //end quote
	
	private function _getSqlCondition($sqlCondition = array())
	{
		return ''; // need mega super reqursion;
	} // end _getSqlCondition
	
	private function _dbQuery($query)
	{
		$link = $this->_doDatabaseConnect();
		
		if (!$link) {
			return false;
		}
		
		$result = mysql_query($query);
		
		if (!$result) {
			$errMsg = __("Mysql Query Exception: %s", mysql_error());
			$errMsg .= PHP_EOL . __("Error Query: %s", $query);
			throw new Exception($errMsg);
		}
		

		$data = $this->_getResultArray($result);
        
		mysql_close($link);

        return $data;        
	} // end _dbQuery
    
    private function _getResultArray($result)
    {
        if (is_bool($result)) {
            return mysql_insert_id();
        }
        
        $resultArray = array();
        
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $resultArray[] = $row;
        }
        
        mysql_free_result($result);
        
        return $resultArray;
    }
	
	private function _doDatabaseConnect()
	{
		$link = mysql_connect(
			    $this->_dbServer, 
				$this->_dbUser, 
				$this->_dbPassword);
		
		if (!$link) {
			$errMsg = __("Database exception: %s", mysql_error());
			throw new Exception($errMsg);
		}
		
		if (!mysql_select_db($this->_dbName)) {
			$errMsg = __("Database '%s' is not found!", $this->_dbName);
			throw new Exception($errMsg);
		}
		
		return $link;
	} // end _doDatabaseConnect
}