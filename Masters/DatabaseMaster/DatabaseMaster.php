<?php 
require_once URL_ROOT . '/core/Masters/DatabaseMaster/Tools/ISqlTool.php';
require_once URL_ROOT . '/core/Masters/DatabaseMaster/SqlObject.php';
require_once URL_ROOT . '/core/Masters/DatabaseMaster/Tools/MysqlTool.php';

class DatabaseMaster extends Entity implements ISqlTool
{
	private $_dbObject = null;
	
	public function inicialize()
	{
		parent::inicialize();
		$this->_prepareTool();
        
        return true;
	} // end inicialize
	
	public function query($sql, $params = array())
	{
		return $this->_dbObject->query($sql, $params);
	} // end query
	
	public function insert($table, $values = array())
	{
		return $this->_dbObject->insert($table, $values);
	} // end insert
	
	public function remove($table, $sqlCondition = array())
	{
		return $this->_dbObject->remove($sql, $sqlCondition);
	} // end remove
	
	public function update($table, $values = array() , $sqlCondition = array())
	{
	    return $this->_dbObject->update($sql, $values, $sqlCondition);	
	} // end update
	
	public function quote($value)
	{
		return $this->_dbObject->quote($value);
	} // end quote
	
	public function massInsert($table, $values = array())
	{
		return $this->_dbObject->massInsert($table, $values);
	} // end massInsert
	
	private function _prepareTool()
	{
		$configInstance = $this->controller->config;
		$dbType = $configInstance->dbType;

		switch ($dbType) {
			case 'mysql' :
				$this->_dbObject = new MysqlTool($configInstance);
			break;
		}
        
        return true;
	} // end _prepareTool
}