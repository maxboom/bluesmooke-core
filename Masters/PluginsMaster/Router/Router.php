<?php 

class Router extends Plugin
{
	public function onInit()
	{
		return $this->_doCallPlugin();
	} // end onInit
	
	private function _doCallPlugin()
	{
        $url = $_SERVER['REQUEST_URI'];
        
        $cleanedUrl = explode('?', $url);
        $url = $cleanedUrl[0];
        
		$query = $this->_loadQueryByUrl($url);

		if (!$query) {
			return false;
		}

        $queryOptions = $this->_getQueryOptionsByUrl($query, $url);
        
        $this->plugins->$query['caption']->onInit();
        
        if (!$this->_prepareOptions($queryOptions)) {
            return $this->plugins->$query['caption']->onAccessDenied(
                $queryOptions['matches'][0]
            );
        }

        return call_user_func_array(array(
           $this->plugins->$query['caption'],
           $query['method'] 
        ), $queryOptions['matches']);
	} // end _doCallPlugin
    
    private function _loadQueryByUrl($url)
    {
        $queryData = $this->object->getByQuery($url);

        if (!$queryData) {
            return false;
        }
        if (!$queryData[0]['id']) {
            return false;
        }
        
        return $queryData[0];
    } // end _loadQueryByUrl
    
	private function _loadQueries()
	{
		return $this->object->getAll();
	} // _loadQueries
    
    private function _prepareOptions(&$queryOptions)
    {
        $guiMaster = $this->controller->masters->GuiMaster;
        $response = &$guiMaster->getProperty("response");
        
        $queryOptions['matches'][0] = &$response;

        return $this->_hasAccesAvailable($queryOptions);
    } // end _prepareOptions
    
    private function _hasAccesAvailable($queryOptions)
    {
        return $this->controller->user->hasAccessToGroups($queryOptions['groups']);
    } // end _hasAccesAvailable
    
    private function _getQueryOptionsByUrl($queryData, $url)
    {
        $options = array();
        
        $pattern = "~" . $queryData['regular'] . "~";   
        preg_match($pattern, $url, $matches);
        
        $options['matches'] = $matches;
        
        if (!$queryData['groups']) {
            $options['groups'] = null;
            return $options;
        }
        
        $options['groups'] = explode(",", $queryData['groups']);
        return $options;
    } // end _getQueryOptionsByUrl
}