<?php 

class Plugin extends PluginObject
{
	public function onInit()
	{
		return true;
	} // end onInit
    
    public function onAccessDenied(Response &$response)
    {
        throw new Exception("Access Denied!");
        return false;
    } // end onAcessDenied
	
	public function fetch($fileName)
	{
        if (!$this->_hasFileExists($fileName, $filePath)) {
            $errMsg = __("File %s not found!", $filePath);
            throw new Exception($errMsg);
        }
        
        ob_start();
        include $filePath;
        return ob_get_clean();
	} // end fetch
	
	public function loadResource($resourceName)
	{
		$fileInfo = new SplFileInfo($resourceName);
        $guiMaster = $this->controller->masters->GuiMaster;
        
        switch ($fileInfo->getExtension()) {
            case 'js' : {
                $guiMaster->addJs($resourceName);
            }
            break;
            case 'css' : {
                $guiMaster->addCss($resourceName);
            }
        }
        
        return true;
	} // end loadResource
    
    private function _hasFileExists($fileName, &$filePath)
    {
        $filePath = $this->_getCurrentDirectoryPath() . 
            "\\template\\" . $fileName;

        if (file_exists($filePath)) {
            return true;
        }
        
        $guiMaster = $this->controller->masters->GuiMaster;
        
        $filePath = $guiMaster->getThemePath() . $fileName;
            
        return file_exists($filePath);
    } // end _hasFileExists
}