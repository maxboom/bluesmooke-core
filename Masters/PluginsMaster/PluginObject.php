<?php 

class PluginObject extends Entity
{
	public $plugins;
	
	public function inicialize()
	{
		parent::inicialize();
		$this->_doLoadPlugins();
		$this->_doLoadDatabaseObject();
        
		return true;
	} // end inicialize
	
	private function _doLoadPlugins()
	{
        $this->plugins = new stdClass();
		$plugins = $this->_getPlugins();
		 
		foreach ($plugins as $pluginPath) {

            $pluginExplode = explode("/", $pluginPath);
            $pluginName = $pluginExplode[count($pluginExplode) - 1];

			if ($this->_hasThisPlugin($pluginName)) {
				continue;
			}

			require_once URL_ROOT . "\\plugins\\"
				. $pluginName . "\\"
				. $pluginName . ".php";
	

			$this->plugins->$pluginName = $pluginName::getInstance();
			//$this->plugins->$pluginName->inicialize();
		}
		
		return true;
	} // end _doLoadPlugins
    
    private function _doLoadDatabaseObject()
    {        
        $pluginName = $this->_getPluginName();
        $objectClassName = $pluginName . "Object";
        $objectPath = dirname($this->_getCurrentPath()) .
            "\\" . $objectClassName . ".php";
        
        if (!file_exists($objectPath)) {
            return false;
        }
        
        require_once $objectPath;
        $this->object = new $objectClassName();
        
        return true;
    } // end _doLoadDatabaseObject
    
    protected function _getPluginName()
    {
        return basename(get_called_class(), ".php");
    } // end _getPluginName
    
    protected function _getCurrentPath()
    {
        $refactor = new ReflectionClass($this);
        return $refactor->getFileName();
    } // end _getCurrentPath
    
    protected function _getCurrentDirectoryPath()
    {
        $refletion = new ReflectionClass(get_called_class());
        return dirname($refletion->getFileName());
    } // end _getCurrentDirectodyPath
	
	private function _getPlugins()
	{
		$pluginPattern = URL_ROOT . "/plugins/*";
		return array_filter(glob($pluginPattern), 'is_dir');
	} // end _getPlugins
	
	private function _hasThisPlugin($pluginName)
	{
		return $pluginName == get_called_class();
	} // end _hasThisPlugin
}