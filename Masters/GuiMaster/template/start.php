<?php

$this->_properties = array(
    'js'         => array(
        "//code.jquery.com/jquery-1.12.0.min.js",
        "//code.jquery.com/ui/1.11.4/jquery-ui.js",
        "/core/Masters/GuiMaster/template/scripts/bluesmoke.js",
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
        "//ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"
    ),
	'css'        => array(
        "//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css",
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css",
        "//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
    ),
    'lang'       => "en-US",
	'caption'    => 'BlueSmoke Framework',
	'meta'       => array(
        'description' => "Free Web Framework",
        'author'      => "zmaxboomz@gmail.com",
        'keywords'    => "php, framework, js, web" 
     ),
    'icon'       => '/core/Masters/GuiMaster/template/bluesmoke.ico',
);