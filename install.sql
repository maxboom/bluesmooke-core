-- MySQL dump 10.10
--
-- Host: localhost    Database: bluesmoke_framework
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bluesmoke_access_group`
--

DROP TABLE IF EXISTS `bluesmoke_access_group`;
CREATE TABLE `bluesmoke_access_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `caption` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ident` (`ident`),
  KEY `ident_2` (`ident`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluesmoke_access_group`
--


/*!40000 ALTER TABLE `bluesmoke_access_group` DISABLE KEYS */;
LOCK TABLES `bluesmoke_access_group` WRITE;
INSERT INTO `bluesmoke_access_group` VALUES (1,'background','Backgound');
UNLOCK TABLES;
/*!40000 ALTER TABLE `bluesmoke_access_group` ENABLE KEYS */;

--
-- Table structure for table `bluesmoke_access_group2users_types`
--

DROP TABLE IF EXISTS `bluesmoke_access_group2users_types`;
CREATE TABLE `bluesmoke_access_group2users_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ident_access_group` varchar(32) NOT NULL,
  `ident_user_type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ident_access_group` (`ident_access_group`,`ident_user_type`),
  KEY `ident_user_type` (`ident_user_type`),
  CONSTRAINT `bluesmoke_access_group2users_types_ibfk_1` FOREIGN KEY (`ident_access_group`) REFERENCES `bluesmoke_access_group` (`ident`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bluesmoke_access_group2users_types_ibfk_2` FOREIGN KEY (`ident_user_type`) REFERENCES `users_types` (`ident`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluesmoke_access_group2users_types`
--


/*!40000 ALTER TABLE `bluesmoke_access_group2users_types` DISABLE KEYS */;
LOCK TABLES `bluesmoke_access_group2users_types` WRITE;
INSERT INTO `bluesmoke_access_group2users_types` VALUES (1,'background','admin');
UNLOCK TABLES;
/*!40000 ALTER TABLE `bluesmoke_access_group2users_types` ENABLE KEYS */;

--
-- Table structure for table `bluesmoke_plugins`
--

DROP TABLE IF EXISTS `bluesmoke_plugins`;
CREATE TABLE `bluesmoke_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `caption` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`caption`),
  KEY `ident` (`ident`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluesmoke_plugins`
--


/*!40000 ALTER TABLE `bluesmoke_plugins` DISABLE KEYS */;
LOCK TABLES `bluesmoke_plugins` WRITE;
INSERT INTO `bluesmoke_plugins` VALUES (1,'test','Test');
UNLOCK TABLES;
/*!40000 ALTER TABLE `bluesmoke_plugins` ENABLE KEYS */;

--
-- Table structure for table `bluesmoke_queries`
--

DROP TABLE IF EXISTS `bluesmoke_queries`;
CREATE TABLE `bluesmoke_queries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ident_plugin` varchar(32) NOT NULL,
  `regular` varchar(256) NOT NULL,
  `method` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ident_plugin` (`ident_plugin`),
  CONSTRAINT `bluesmoke_queries_ibfk_3` FOREIGN KEY (`ident_plugin`) REFERENCES `bluesmoke_plugins` (`ident`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluesmoke_queries`
--


/*!40000 ALTER TABLE `bluesmoke_queries` DISABLE KEYS */;
LOCK TABLES `bluesmoke_queries` WRITE;
INSERT INTO `bluesmoke_queries` VALUES (2,'test','^/test/([0-9]+)/([A-Za-z]+)/$','test'),(3,'test','^/access/test/$','empty');
UNLOCK TABLES;
/*!40000 ALTER TABLE `bluesmoke_queries` ENABLE KEYS */;

--
-- Table structure for table `bluesmoke_queries2access_group`
--

DROP TABLE IF EXISTS `bluesmoke_queries2access_group`;
CREATE TABLE `bluesmoke_queries2access_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_query` int(11) unsigned NOT NULL,
  `ident_access_group` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_query` (`id_query`,`ident_access_group`),
  KEY `ident_access_group` (`ident_access_group`),
  CONSTRAINT `bluesmoke_queries2access_group_ibfk_1` FOREIGN KEY (`ident_access_group`) REFERENCES `bluesmoke_access_group` (`ident`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bluesmoke_queries2access_group_ibfk_2` FOREIGN KEY (`id_query`) REFERENCES `bluesmoke_queries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluesmoke_queries2access_group`
--


/*!40000 ALTER TABLE `bluesmoke_queries2access_group` DISABLE KEYS */;
LOCK TABLES `bluesmoke_queries2access_group` WRITE;
INSERT INTO `bluesmoke_queries2access_group` VALUES (1,2, 'background');
UNLOCK TABLES;
/*!40000 ALTER TABLE `bluesmoke_queries2access_group` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `ident_user_type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ident_user_type` (`ident_user_type`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ident_user_type`) REFERENCES `users_types` (`ident`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

--
-- Table structure for table `users_types`
--

DROP TABLE IF EXISTS `users_types`;
CREATE TABLE `users_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `caption` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ident` (`ident`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_types`
--


/*!40000 ALTER TABLE `users_types` DISABLE KEYS */;
LOCK TABLES `users_types` WRITE;
INSERT INTO `users_types` VALUES (1,'admin','Admin');
UNLOCK TABLES;
/*!40000 ALTER TABLE `users_types` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

