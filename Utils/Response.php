<?

class Response
{
    const ACTION_VIEW = "view";
    const ACTION_REDIRECT = "redirect";
    const ACTION_RELOAD = "reload";
    const ACTION_MESSAGE = "message";
    const ACTION_AJAX = "ajax";
    const ACTION_EXCEPTION = "exception";
    
    public $_options = array();
    
    public function __construct(
        $action = Response::ACTION_VIEW, $options = array()
    )
    {
        $this->_options['action'] = $action;
        $this->_options['content'] = "";
        $this->_options = array_merge($this->_options, $options);
        
        return true;
    } // end __construct
    
    public function setOptions($options = array())
    {
        $this->_options = array_merge($options, $this->_options);
        return true;
    } // end setOptions
    
    public function setOption($optionName, $value)
    {
        $this->_options[$optionName] = $value;
        return true;
    } // end setOption
    
    public function getOption($optionName)
    {
        if (!array_key_exists($optionName, $this->_options)) {
            return false;
        }
        return $this->_options[$optionName];
    } // end getOption
    
    public function setMessage($caption)
    {
        $this->setOption("action", Response::ACTION_MESSAGE);
        $this->setOption("message", $caption);
    
        return true;
    } // end setMessage
    
    public function setRedirect($url)
    {
        $this->setOption("action", Response::ACTION_REDIRECT);
        $this->setOption("url", $url);
        
        return true;
    } // end setRedirect
    
    public function setContent($content)
    {
        $this->setOption("action", Response::ACTION_VIEW);
        $this->setOption("content", $content);
        
        return true;
    } // end setContent

    public function setException(
        $message, $severity, $file = null, $line = null
    )
    {
        $this->setOption("action", Response::ACTION_EXCEPTION);
        $this->setOption("message", $message);
        $this->setOption("severity", $severity);
        $this->setOption("file", $file);
        $this->setOption("line", $line);
        
        return true;
    }
}